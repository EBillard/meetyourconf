# MeetYourConf

Projet web Licence Professionnelle DAWIN
Conception / Réalisation d'un site web présentant les conférences web se déroulant en France.
[MeetYourConf.fr](http://www.meetyourconf.fr)

### Description du projet

Ce projet est réalisé avec le framework PHP Laravel et le framework css Bootstrap.

MeetYourConf est un annuaire qui liste les conférences liées à l'informatique et aux nouvelles technologies.
Toute personne possédant un compte peut proposer une nouvelle conférence. La proposition sera ensuite traitée par un modérateur qui la validera ou non.

Vous retrouverez dans cet annuaire des conférences comme BDX/IO, web2days, Parisweb ... 

### Ce projet est réalisé par (Nom Prénom *role* (origine)):

Billard Emilien *chef de projet / développer back end* (DUT informatique, Limoges)

Prevot Cyril *développer front/back end* (BTS SIO, Bordeaux)

Mucignato Estelle *responsable design/développer front end* (DUT informatique, Bordeaux)

Ahmadi Abasse *développer front/back end* (BTS SIO, Périgueux)

Ben Halima Malek *développer front end* (Licence Informatique, Tunisie)